\name{Techniques 1-4: Internal Functions}
\alias{testP-internal}
\alias{aa_graphATestsForSampleSize}
\alias{atest}
\alias{normaliseATest}
\alias{pcor.mat}
\alias{pcor.rec}
\alias{pcor.test}
\alias{lhc_constructCoEffDataSet}
\alias{efast_cvmethod}
\alias{efast_graph_Results}
\alias{efast_parameterdist}
\alias{efast_sd}
\alias{efast_setfreq}
\alias{efast_ttest}
\alias{table_header_check}
\alias{num.decimals}
\alias{perform_aTest_for_all_sim_measures}
\alias{prepare_parameter_value_list}
\alias{subset_results_by_param_value_set}
\docType{package}
\title{Internal Functions}
\description{A number of internal functions (14) are used which are not directly called by the user employing this package.  These are: (a) aa_getATestResults (b) aa_graphATestsForSampleSize (c) atest (d) normaliseATest (e) pcor.mat (f) pcor.rec (g) pcor.test (h) lhc_constructCoEffDataSet (i) efast_cvmethod (j) efast_graph_Results (k) efast_parameterdist (l) efast_sd (m) efast_setfreq (n) efast_ttest (o) table_header_check (p) num_decimals
 }
